﻿using SenaCar.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SenaCar.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DescricaoView : ContentPage
    {
        private const float Vidros_Elétricos = 545;
        private const float Travas_Elétricas = 260;
        private const float Ar_Condicionado = 480;
        private const float Camera_de_Ré = 180;
        private const float Cambio = 460;
        private const float Suspensão = 380;
        private const float Freios = 245;


        public string TextoVidrosEletricos
        {
            get
            {
                return string.Format("Vidros Elétricos - R$ {0}", Vidros_Elétricos);
            }

        }

        public string TextoTravasEletricas
        {
            get
            {
                return string.Format("Travas Elétricas - R$ {0}", Travas_Elétricas);
            }

        }

        public string TextoArCondicionado
        {
            get
            {
                return string.Format("Ar Condicionado - R$ {0}", Ar_Condicionado);
            }

        }

        public string TextoCameradeRe
        {
            get
            {
                return string.Format("Camera de Ré - R$ {0}", Camera_de_Ré);
            }

        }
        public string TextoCambio
        {
            get
            {
                return string.Format("Cambio - R$ {0}", Cambio);
            }

        }
        public string TextoSuspensao
        {
            get
            {
                return string.Format("Suspensão - R$ {0}", Suspensão);
            }

        }
        public string TextoFreios
        {
            get
            {
                return string.Format("Freios - R$ {0}", Freios);
            }

        }

        bool incluiVidros_Elétricos;

        public bool IncluiVidros_Elétricos
        {

            get
            {
                return incluiVidros_Elétricos;
            }
            set
            {
                incluiVidros_Elétricos = value;
                if (incluiVidros_Elétricos)
                    DisplayAlert("Vidros Elétricos", "Ativo", "ok");

                else DisplayAlert("Vidros Elétricos", "Inativo", "ok");
            }

        }

        bool incluiTravas_Elétricas;
        public bool IncluiTravas_Elétricas
        {

            get
            {
                return incluiTravas_Elétricas;
            }
            set
            {
                incluiTravas_Elétricas = value;
                if (incluiTravas_Elétricas)
                    DisplayAlert("Travas Elétricas", "Ativo", "ok");

                else DisplayAlert("Travas Elétricas", "Inativo", "ok");
            }

        }
        bool incluiAr_Condicionado;

        public bool IncluiAr_Condicionado
        {

            get
            {
                return incluiAr_Condicionado;
            }
            set
            {
                incluiAr_Condicionado = value;
                if (incluiAr_Condicionado)
                    DisplayAlert("Ar Condicionado", "Ativo", "ok");

                else DisplayAlert("Ar Condicionado", "Inativo", "ok");
            }

        }
        bool incluiCamera_de_Ré;
        public bool IncluiCamera_de_Ré
        {

            get
            {
                return incluiCamera_de_Ré;
            }
            set
            {
                incluiCamera_de_Ré = value;
                if (incluiCamera_de_Ré)
                    DisplayAlert("Camera de Ré", "Ativo", "ok");

                else DisplayAlert("Camera de Ré", "Inativo", "ok");
            }

        }
        bool incluiCambio;
        public bool IncluiCambio
        {

            get
            {
                return incluiCambio;
            }
            set
            {
                incluiCambio = value;
                if (incluiCambio)
                    DisplayAlert("Cambio", "Ativo", "ok");

                else DisplayAlert("Cambio", "Inativo", "ok");
            }

        }
        bool incluiSuspensao;
        public bool IncluiSuspensao
        {

            get
            {
                return incluiSuspensao;
            }
            set
            {
                incluiSuspensao = value;
                if (incluiSuspensao)
                    DisplayAlert("Suspensao", "Ativo", "ok");

                else DisplayAlert("Suspensao", "Inativo", "ok");
            }

        }
        bool incluiFreios;
        public bool IncluiFreios
        {

            get
            {
                return incluiFreios;
            }
            set
            {
                incluiFreios = value;
                if (incluiFreios)
                    DisplayAlert("Freios", "Ativo", "ok");

                else DisplayAlert("Freios", "Inativo", "ok");
            }

        }

        public Servico Servicos { get; set; }
        public DescricaoView(Servico servicos)
        {
            InitializeComponent();

            this.Title = servicos.Modelo;
            this.Servicos = servicos;
            this.BindingContext = this;
        }

        private void ButtonProximo_Clicked(object sender, EventArgs e)
        {
          Navigation.PushAsync(new AgendamentoView(this.Servicos));

        }

    }

}